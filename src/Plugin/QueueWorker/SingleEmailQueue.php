<?php

namespace Drupal\forum_notifications_subscription\Plugin\QueueWorker;

use Drupal\Core\Mail\MailManager;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Queue\QueueWorkerBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Send queued single messages.
 *
 * @QueueWorker(
 * id = "single_email_queue",
 * title = @Translation("Forum subscription single email queue processor"),
 * cron = {"time" = 90}
 * )
 */
class SingleEmailQueue extends QueueWorkerBase implements ContainerFactoryPluginInterface {

  /**
   * The mail manager.
   *
   * @var \Drupal\Core\Mail\MailManager
   */
  private $mail;

  /**
   * Queue worker constructor.
   *
   * @param array $configuration
   *   The plugin configuration.
   * @param string $plugin_id
   *   The plugin ID.
   * @param mixed $plugin_definition
   *   The plugin definition.
   * @param \Drupal\Core\Mail\MailManager $mail
   *   The mail manager.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, MailManager $mail) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->mail = $mail;
  }

  /**
   * {@inheritdoc}
   */
  static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('plugin.manager.mail')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function processItem($data) {
    $params = $data;
    $this->mail->mail('forum_notifications_subscription', 'single_email_queue', $data['to'], $data['langcode'], $params, NULL, TRUE);
  }
}