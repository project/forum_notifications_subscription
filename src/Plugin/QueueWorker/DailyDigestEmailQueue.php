<?php

namespace Drupal\forum_notifications_subscription\Plugin\QueueWorker;

use Drupal\Core\Mail\MailManager;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Queue\DelayedRequeueException;
use Drupal\Core\Queue\QueueWorkerBase;
use Drupal\user\Entity\User;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Send queued daily digest emails.
 *
 * @QueueWorker(
 * id = "daily_digest_email_queue",
 * title = @Translation("Forum subscription daily digest email queue processor"),
 * cron = {"time" = 100}
 * )
 */
class DailyDigestEmailQueue extends QueueWorkerBase implements ContainerFactoryPluginInterface {

  /**
   * The mail manager.
   *
   * * @var \Drupal\Core\Mail\MailManager
   */
  private $mail;

  /**
   * Queue worker constructor.
   *
   * @param array $configuration
   *   The plugin configuration.
   * @param string $plugin_id
   *   The plugin ID.
   * @param mixed $plugin_definition
   *   The plugin definition.
   * @param \Drupal\Core\Mail\MailManager $mail
   *   The mail manager.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, MailManager $mail) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->mail = $mail;
  }

  /**
   * {@inheritdoc}
   */
  static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('plugin.manager.mail')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function processItem($data) {
    $automated_cron_settings = \Drupal::config('automated_cron.settings');
    $cron_interval = $automated_cron_settings->get('interval') / 3600;
    $from_middle_night = $this->getInterval($data['user_id']);
    if ($from_middle_night <= $cron_interval) {
      $params['to'] = $data['to'];
      $params['title'] = $data['title'];
      $params['message'] = $data['message'];
      $this->mail->mail('forum_notifications_subscription', 'daily_digest_email_queue', $data['to'], $data['langcode'], $params, NULL, TRUE);
    }
    else {
      throw new DelayedRequeueException;
    }
  }

  /**
   * Get the number of hours before middle night of the user time zone.
   * @param $user_id
   *
   * @return string
   * @throws \Exception
   */
  private function getInterval($user_id) {
    $user = User::load($user_id);
    $time_zone = $user->getTimeZone();
    $date = new \DateTime("now", new \DateTimeZone($time_zone));
    $tomorrow_middle_night = new \DateTime('tomorrow midnight', new \DateTimeZone($time_zone));
    $diff = date_diff($date, $tomorrow_middle_night);

    return $diff->format('%h');
  }
}