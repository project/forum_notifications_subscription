<?php

namespace Drupal\forum_notifications_subscription\Controller;

use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\ReplaceCommand;
use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Url;

/**
 * Controller responses subscribe and unsubsribe action links.
 *
 * The response is a set of AJAX commands to update the
 * link in the page.
 */
class ActionLinkController extends ControllerBase implements ContainerInjectionInterface {

  public function subscribe(string $entity_type_id = NULL, string $entity_id = NULL) {
    \Drupal::service('forum_notifications_subscription.frequency')
      ->createNotificationFrequencyByEntity($entity_id, $entity_type_id);
    return $this->getResponse($entity_type_id, $entity_id);
  }

  public function unsubscribe(string $entity_type_id = NULL, string $entity_id = NULL) {
    \Drupal::service('forum_notifications_subscription.frequency')
      ->deleteNotificationFrequencyByEntityAndType($entity_id, $entity_type_id);
    return $this->getResponse($entity_type_id, $entity_id);
  }

  public static function getSubscriptionLink(ContentEntityInterface $entity, string $type, string $link_selector): array {
    $entity_type_id = $entity->getEntityTypeId();
    $label_setting_key = $entity_type_id == 'node' ? 'topic' : 'forum';
    $subscription_frequency = \Drupal::service('forum_notifications_subscription.frequency');
    $config = \Drupal::config('forum_notifications_subscription.settings');
    if ($subscription_frequency->currentUserHasNotificationFrequencyByEntity($entity, $type)) {
      $route_name = 'forum_notifications_subscription.action_link_unsubscription';
      $title = $config->get($label_setting_key . '_label_off');
    }
    else {
      $route_name = 'forum_notifications_subscription.action_link_subscription';
      $title = $config->get($label_setting_key . '_label_on');
    }

    $link = [
      '#type' => 'link',
      '#title' => $title,
      '#url' => Url::fromRoute($route_name, [
        'entity_type_id' => $entity_type_id,
        'entity_id' => $entity->id(),
      ]),
      '#attributes' => [
        'id' => $link_selector,
        'class' => ['use-ajax', 'btn', 'btn-primary'],
      ],
    ];

    $cacheable_metadata = new CacheableMetadata();
    $cacheable_metadata->setCacheContexts(['route']);
    $cacheable_metadata->addCacheableDependency($entity);
    $cacheable_metadata->addCacheableDependency($config);
    $cacheable_metadata->addCacheableDependency(\Drupal::currentUser());
    $cacheable_metadata->applyTo($link);
    return $link;
  }

  protected function getResponse(string $entity_type_id, string $entity_id) {
    $entity = \Drupal::entityTypeManager()->getStorage($entity_type_id)->load($entity_id);
    if ($entity_type_id == 'node') {
      $wrapper_class = 'forum-topic-subscription';
      $type = 'Forum topic';
    }
    else {
      $wrapper_class = 'forum-main-subscription';
      $type = 'Forum';
    }

    $link_selector = $wrapper_class . '-' . $entity->id();
    $response = new AjaxResponse();
    $response->addCommand(new ReplaceCommand("#{$link_selector}", static::getSubscriptionLink($entity, $type, $link_selector)));
    return $response;
  }

}
